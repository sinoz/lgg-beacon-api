import { Config } from "./config.js";
import { createMetrics, Metrics } from "./metrics.js";
import { createServices, Services } from "./service.js";

export interface Context {
    config: Config;
    services: Services;
    metrics: Metrics;
    signal: AbortSignal;
    destroy: () => Promise<void>;
}

export function createContext(
    config: Config,
): Context {
    const services = createServices(config);

    const metrics = createMetrics(config.promRegistry);
    const signal = config.abortController.signal;

    const destroy = async () => {
        await services.pgPool.end();
    };

    return {
        config,
        services,
        metrics,
        signal,
        destroy,
    };
}
