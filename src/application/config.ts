import { HttpSendReceive } from "@oas3/http-send-receive";
import * as prom from "prom-client";

export interface Config {
    endpoint: URL;

    pgUri: URL;
    authApiEndpoint: URL;

    abortController: AbortController;
    promRegistry: prom.Registry;
    httpSendReceive?: HttpSendReceive;
}
