import * as metaApi from "@latency.gg/lgg-meta-api";
import { createHttpAgentSendReceive } from "@oas3/http-send-receive";
import { isAbortError, waitForAbort } from "abort-tools";
import { program } from "commander";
import delay from "delay";
import * as http from "http";
import { second } from "msecs";
import * as prom from "prom-client";
import { WaitGroup } from "useful-wait-group";
import * as application from "../application/index.js";

program.
    command("server").
    option("--port <number>", "Listen to port", Number, 8080).
    option(
        "--endpoint <url>",
        "endpoint of the service",
        v => new URL(v),
        new URL("http://localhost:8080"),
    ).
    option("--meta-port <number>", "Listen to meta-port", Number, 9090).
    option(
        "--meta-endpoint <url>",
        "endpoint of the meta-service",
        v => new URL(v),
        new URL("http://localhost:9090"),
    ).
    option("--pg-uri <url>", "pg uri", v => new URL(v)).
    option(
        "--auth-api-endpoint <url>",
        "auth-api-endpoint",
        v => new URL(v),
        new URL("http://lgg-auth-api:8080"),
    ).
    option("--shutdown-delay <msec>", "", Number, 35 * second).
    option("--connect-timeout <msec>", "", Number, 10 * second).
    option("--idle-timeout <msec>", "", Number, 40 * second).
    action(programAction);

interface ActionOptions {
    port: number;
    metaPort: number;
    endpoint: URL;
    metaEndpoint: URL;
    pgUri: URL;
    authApiEndpoint: URL;
    shutdownDelay: number;
    connectTimeout: number;
    idleTimeout: number;
}
async function programAction(actionOptions: ActionOptions) {
    const {
        port,
        endpoint,
        metaPort,
        metaEndpoint,
        pgUri,
        authApiEndpoint,
        shutdownDelay,
        connectTimeout,
        idleTimeout,
    } = actionOptions;

    console.log("starting...");

    process.
        on("uncaughtException", error => {
            console.error(error);
            process.exit(1);
        }).
        on("unhandledRejection", error => {
            console.error(error);
            process.exit(1);
        });

    const onWarn = (error: unknown) => {
        console.warn(error);
    };

    const livenessController = new AbortController();
    const readinessController = new AbortController();

    const promRegistry = new prom.Registry();
    prom.collectDefaultMetrics({ register: promRegistry });

    {
        /*
        When we receive a signal, the server should be not ready anymore, so
        we abort the readiness controller
        */
        const onSignal = (signal: NodeJS.Signals) => {
            console.log(signal);
            readinessController.abort();
        };
        process.addListener("SIGINT", onSignal);
        process.addListener("SIGTERM", onSignal);
    }

    const metaConfig: metaApi.Config = {
        endpoint: metaEndpoint,

        livenessController,
        readinessController,

        promRegistry,
    };
    const metaContext = metaApi.createContext(metaConfig);

    const config: application.Config = {
        endpoint,

        pgUri,
        authApiEndpoint,

        abortController: livenessController,
        promRegistry,

        httpSendReceive: createHttpAgentSendReceive({
            connectTimeout,
            idleTimeout,
        }),
    };

    const applicationContext = application.createContext(config);
    try {

        /*
        We use the waitgroup to wait for requests to finish
        */
        const waitGroup = new WaitGroup();

        const applicationServer = application.createServer(
            applicationContext,
            onWarn,
        );
        applicationServer.registerMiddleware(
            (route, request, next) => waitGroup.with(() => next(request)),
        );

        const metaServer = metaApi.createServer(
            metaContext,
            onWarn,
        );
        metaServer.registerMiddleware(
            (route, request, next) => waitGroup.with(() => next(request)),
        );

        const applicationHttpServer = http.createServer(applicationServer.asRequestListener({
            onError: onWarn,
        }));
        const metaHttpServer = http.createServer(metaServer.asRequestListener({
            onError: onWarn,
        }));

        await new Promise<void>(resolve => applicationHttpServer.listen(
            port,
            () => resolve(),
        ));
        try {
            await new Promise<void>(resolve => metaHttpServer.listen(
                metaPort,
                () => resolve(),
            ));

            try {

                console.log("started");

                try {
                    await waitForAbort(readinessController.signal, "application aborted");
                }
                catch (error) {
                    if (!isAbortError(error)) throw error;
                }

                console.log("stopping...");

                /*
                wait a bit, usually for the load balancer to recognize this service as
                not ready and move traffic to another service.
                TODO: implement heartbeat so this will actually work!
                */
                await delay(shutdownDelay);

                /*
                wait for all requests to finish
                */
                await waitGroup.wait();

                /*
                abort the liveness controller to terminate all streaming responses!
                */
                livenessController.abort();

            }
            finally {
                await new Promise<void>((resolve, reject) => metaHttpServer.close(
                    error => error ?
                        reject(error) :
                        resolve(),
                ));
            }
        }
        finally {
            await new Promise<void>((resolve, reject) => applicationHttpServer.close(
                error => error ?
                    reject(error) :
                    resolve(),
            ));
        }

    }
    finally {
        await applicationContext.destroy();
    }

    console.log("stopped");
}
