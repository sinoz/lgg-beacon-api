import * as beaconApi from "@latency.gg/lgg-beacon-oas";
import assert from "assert";
import * as crypto from "crypto";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createRegisterBeaconOperation(
    context: application.Context,
): beaconApi.RegisterBeaconOperationHandler<application.ServerAuthorization> {
    return async (
        incomingRequest,
        authorization,
        acceptTypes,
    ) => {
        const { client } = authorization.basic;
        const beaconEntity = await incomingRequest.entity();

        const beacon = crypto.randomBytes(24);
        const {
            version,
            ipv4, ipv6,
            provider: providerLabel, location: locationLabel,
        } = beaconEntity;

        const now = new Date();

        await withPgTransaction(
            context,
            "insert-beacon",
            async pgClient => {
                let location;
                let result;

                result = await pgClient.query(`
select l.location
from public.location_label as l
inner join public.location_label as p
on l.location = p.location
where l.name = 'location'
and p.name = 'provider'
and l.value = $1
and p.value = $2
;
`, [locationLabel, providerLabel]);

                if (result.rowCount > 0) {
                    assert(result.rowCount === 1);

                    location = result.rows[0].location;
                }
                else {
                    location = crypto.randomBytes(24);

                    result = await pgClient.query(`
insert into public.location(id, created_utc)
values($1, $2)
returning 1
;
                    `, [location, now.toISOString()]);
                    assert.equal(result.rowCount, 1);

                    result = await pgClient.query(`
insert into public.location_label(location, name, value)
values($1, $2, $3)
returning 1
;
`, [location, "location", locationLabel]);
                    assert.equal(result.rowCount, 1);

                    result = await pgClient.query(`
insert into public.location_label(location, name, value)
values($1, $2, $3)
returning 1
;
`, [location, "provider", providerLabel]);
                    assert.equal(result.rowCount, 1);
                }

                result = await pgClient.query(`
INSERT INTO public.beacon (
    id,
    location, client,
    ipv4, ipv6,
    version,
    created_utc, last_seen_utc,
    public_key
)
VALUES (
    $1,
    $2, $3,
    $4, $5,
    $6,
    $7, $7,
    $8
)
RETURNING 1
;
`, [
                    beacon,
                    location, client,
                    ipv4, ipv6,
                    version,
                    now.toISOString(),
                    beaconEntity.publickey == null ?
                        null :
                        Buffer.from(beaconEntity.publickey, "base64"),
                ]);
                assert.equal(result.rowCount, 1);
            },
        );

        return {
            status: 202,
            parameters: {},
            entity() {
                return {
                    beacon: beacon.toString("base64"),
                    ipv4, ipv6,
                    location: locationLabel,
                    provider: providerLabel,
                    version,
                };
            },
        };
    };
}
