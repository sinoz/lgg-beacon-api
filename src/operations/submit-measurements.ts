import * as beaconApi from "@latency.gg/lgg-beacon-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import assert from "assert";
import * as tweetnacl from "tweetnacl-ts";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createSubmitMeasurementsOperation(
    context: application.Context,
): beaconApi.SubmitMeasurementsOperationHandler<application.ServerAuthorization> {
    return async (
        incomingRequest,
        authorization,
        acceptTypes,
    ) => {
        const { client } = authorization.basic;
        const { beacon } = incomingRequest.parameters;
        const measurementEntities = await incomingRequest.entity();

        const now = new Date();

        await withPgTransaction(
            context,
            "update-last-seen",
            async pgClient => {
                const result = await pgClient.query(`
update public.beacon
set last_seen_utc = $2
where id = $1
;
`,
                    [
                        Buffer.from(beacon, "base64"),
                        now.toISOString(),
                    ],
                );
            },
        );

        await withPgTransaction(
            context,
            "insert-measurement",
            async pgClient => {
                let result;

                result = await pgClient.query(`
select public_key
from public.beacon
where id = $1
;
`,
                    [
                        Buffer.from(beacon, "base64"),
                    ],
                );

                assert.equal(result.rowCount, 1);

                const publicKeyBuffer = result.rows[0].public_key;

                for (const measurementEntity of measurementEntities) {
                    assert.equal(measurementEntity.type, "udp-data");

                    let timestamp;
                    if (measurementEntity.timestamp != null) {
                        timestamp = new Date(measurementEntity.timestamp);
                    }
                    if (measurementEntity.raw != null) {
                        timestamp = new Date(measurementEntity.raw.timestamp_c0);
                    }
                    else {
                        timestamp = now;
                    }

                    const source = measurementEntity.ip;

                    const actualToken = measurementEntity.token;

                    if (actualToken) {
                        const encoder = new TextEncoder();
                        const messageBuffer = encoder.encode(timestamp.toISOString() + "/" + source);

                        const expectedTokenBuffer = Buffer.from(tweetnacl.sealedbox(
                            messageBuffer,
                            publicKeyBuffer,
                        ));
                        const expectedToken = expectedTokenBuffer.toString("base64");

                        if (actualToken !== expectedToken) {
                            throw new oas3ts.AuthorizationFailedError("server", "submit-measurements");
                        }
                    }

                    result = await pgClient.query(`
insert into public.measurement(
    timestamp_utc, source, client
)
values(
    $1, $2, $3
)
on conflict do nothing
;
`, [timestamp.toISOString(), source, client]);

                    result = await pgClient.query(`
insert into public.sample_udp_data (
    timestamp_utc, source, beacon,
    rtt_ms, stddev, raw
)
values(
    $1, $2, $3,
    $4, $5, $6
)
returning 1
;
`,
                        [
                            timestamp.toISOString(), source, Buffer.from(beacon, "base64"),
                            measurementEntity.rtt, measurementEntity.stddev, measurementEntity.raw,
                        ],
                    );
                    assert.equal(result.rowCount, 1);
                }
            },
        );

        return {
            status: 202,
            parameters: {},
            entity() {
                return measurementEntities;
            },
        };
    };
}
