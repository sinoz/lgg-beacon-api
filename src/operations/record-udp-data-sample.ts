import * as beaconApi from "@latency.gg/lgg-beacon-oas";
import assert from "assert";
import * as application from "../application/index.js";
import { withPgTransaction } from "../utils/index.js";

export function createRecordUdpDataSampleOperation(
    context: application.Context,
): beaconApi.RecordUdpDataSampleOperationHandler<application.ServerAuthorization> {
    return async (
        incomingRequest,
        authorization,
    ) => {
        const now = new Date();

        const source = incomingRequest.parameters.source;
        const beaconId = incomingRequest.parameters.beacon;
        const beaconIdBuffer = Buffer.from(beaconId, "base64");

        const incomingEntity = await incomingRequest.entity();
        const timestamp = new Date(incomingEntity.timestamp);

        await withPgTransaction(
            context,
            "insert-sample-udp-data",
            async pgClient => {
                let result;

                result = await pgClient.query(`
update public.beacon
set last_seen_utc = $2
where id = $1
returning 1
;
`,
                    [
                        beaconIdBuffer,
                        now.toISOString(),
                    ],
                );
                assert.equal(result.rowCount, 1);

                result = await pgClient.query(`
insert into public.sample_udp_data (
    timestamp_utc, source, beacon,
    rtt_ms, stddev
)
values(
    $1, $2, $3,
    $4, $5
)
;
`,
                    [
                        timestamp.toISOString(), source, beaconIdBuffer,
                        incomingEntity.rttAvg, incomingEntity.rttSd,
                    ],
                );

            },

        );

        return {
            status: 204,
            parameters: {},
        };
    };
}
